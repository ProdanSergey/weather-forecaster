import colors from 'vuetify/es5/util/colors';

export default {
  primary: {
    base: colors.purple.base,
    darken1: colors.purple.darken2
  },
  tertiary: colors.pink.base
}