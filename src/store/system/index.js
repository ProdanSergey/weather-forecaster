import { INITIALIZE_APP } from './mutation-types'

import ls from 'local-storage'

const state = {
  isAppInitialize: false
}

const actions = {
  initialize: async ({ dispatch, commit }) => {
    const userID = ls('user')
    if (userID) await dispatch('user/loginUser', { userID }, { root: true })
    await dispatch('user/setGeolocation', undefined, { root: true })

    commit(INITIALIZE_APP)
  }
}

const mutations = {
  [INITIALIZE_APP](state) {
    state.isAppInitialize = true
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
