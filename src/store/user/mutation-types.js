export const SET_USER_DATA = 'setUserData'
export const REMOVE_USER_DATA = 'removeUserData'

export const START_USER_FETCH = 'startUserFetch'
export const FINISH_USER_FETCH = 'finishUserFetch'

export const SET_GEOLOCATION = 'setGeoLocation'
export const START_GEOLOCATION_SEARCH = 'startGeolocationSearch'
export const FINISH_GEOLOCATION_SEARCH = 'finishGeolocationSearch'

export const SET_SUBSCRIBTION = 'setSubscribtion'
export const REMOVE_SUBSCRIBTION = 'removeSubscribtion'
export const SET_THEME = 'setTheme'
export const SET_METRIC = 'setMetric'
export const SET_GPS = 'setGPS'
