import {
  START_USER_FETCH,
  FINISH_USER_FETCH,
  SET_USER_DATA,
  REMOVE_USER_DATA,
  SET_SUBSCRIBTION,
  REMOVE_SUBSCRIBTION,
  SET_GEOLOCATION,
  SET_THEME,
  SET_METRIC,
  SET_GPS,
  START_GEOLOCATION_SEARCH,
  FINISH_GEOLOCATION_SEARCH
} from './mutation-types'

import {
  setNewDBEntry,
  getDBEntry,
  getDBEntryByIndex
} from '@/assets/js/indexedDB'

import { getLocationsByGeo } from '@/assets/js/apiCalls'

import ls from 'local-storage'
import { getCurrentPosition } from '@/assets/js/geolocation'
import { METRICS } from '@/assets/constants'

import pick from 'lodash/pick'

const state = {
  isLogin: false,
  isUserFetching: false,
  isDarkTheme: false,
  metric: METRICS.Celsius.VALUE,
  subscribtions: [],
  geo: {
    isGPSAllowed: true,
    isLoading: false,
    points: []
  },
  userData: {}
}

const getters = {
  getNearestGeoPoint: state => {
    if (state.geo.points.length) {
      return state.geo.points.reduce((last, next) => {
        return last.distance < next.distance ? last : next
      })
    }
  }
}

const actions = {
  getUserById: ({ commit }, userID) => {
    commit(START_USER_FETCH)
    return getDBEntry('user', userID)
      .then(({ result }) => {
        if (result) {
          commit(SET_USER_DATA, result)
          return result
        } else {
          throw new Error("user dosn't exist")
        }
      })
      .finally(() => commit(FINISH_USER_FETCH))
  },
  getUserByEmail: ({ commit }, { email, password }) => {
    commit(START_USER_FETCH)
    return getDBEntryByIndex('user', 'email', email)
      .then(({ result: user }) => {
        if (!user) {
          throw new Error("user with provided email dosn't exist")
        } else if (user && user.password !== password) {
          throw new Error('wrong password')
        } else {
          ls('user', user.id)
          commit(SET_USER_DATA, user)
          return user
        }
      })
      .finally(() => commit(FINISH_USER_FETCH))
  },
  setNewUser: ({ commit }, user) => {
    commit(START_USER_FETCH)
    return setNewDBEntry('user', user)
      .then(user => {
        commit(SET_USER_DATA, user)
        ls('user', user.id)
        return user
      })
      .finally(() => commit(FINISH_USER_FETCH))
  },
  loginUser: ({ dispatch, state }, user) => {
    if (user.userID && !state.isLogin) {
      return dispatch('getUserById', user.userID)
    }
    if (user.email && user.password) {
      return dispatch('getUserByEmail', pick(user, ['email', 'password']))
    }
  },
  logoutUser: ({ commit }) => {
    ls.remove('user')
    commit(REMOVE_USER_DATA)
  },
  setSubscribtion: async ({ commit }, locationId) => {
    commit(SET_SUBSCRIBTION, locationId)
  },
  removeSubscribtion: ({ commit }, locationId) => {
    commit(REMOVE_SUBSCRIBTION, locationId)
  },
  setMetric: ({ commit }, metric) => {
    commit(SET_METRIC, metric)
  },
  setTheme: ({ commit }, state) => {
    commit(SET_THEME, state)
  },
  setGPS: ({ commit }, state) => {
    commit(SET_GPS, state)
  },
  setGeolocation: async ({ commit, getters }) => {
    if (!getters.getNearestGeoPoint) {
      commit(START_GEOLOCATION_SEARCH)
      try {
        const geo = await getCurrentPosition()
        const locations = await getLocationsByGeo(geo.latitude, geo.longitude)
        const points = locations.map(location => ({ ...location, isGPS: true }))
        commit(SET_GEOLOCATION, points)
      } catch (error) {
        throw new Error("Can't detect your location")
      } finally {
        commit(FINISH_GEOLOCATION_SEARCH)
      }
    }
  }
}

const mutations = {
  [SET_USER_DATA](state, payload) {
    state.isLogin = true
    state.userData = payload
  },
  [REMOVE_USER_DATA](state) {
    state.isLogin = false
    state.userData = {}
  },
  [START_USER_FETCH](state) {
    state.isUserFetching = true
  },
  [FINISH_USER_FETCH](state) {
    state.isUserFetching = false
  },
  [SET_GEOLOCATION](state, payload) {
    state.geo = {
      ...state.geo,
      points: [...payload]
    }
  },
  [START_GEOLOCATION_SEARCH](state) {
    state.geo.isLoading = true
  },
  [FINISH_GEOLOCATION_SEARCH](state) {
    state.geo.isLoading = false
  },
  [SET_SUBSCRIBTION](state, payload) {
    state.subscribtions = [...state.subscribtions, payload]
  },
  [REMOVE_SUBSCRIBTION](state, payload) {
    state.subscribtions = state.subscribtions.filter(
      locationId => locationId !== payload
    )
  },
  [SET_THEME](state, payload) {
    state.isDarkTheme = payload
  },
  [SET_METRIC](state, payload) {
    state.metric = payload
  },
  [SET_GPS](state, payload) {
    state.geo.isGPSAllowed = payload
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
