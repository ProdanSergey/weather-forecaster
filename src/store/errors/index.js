import { HANDLE_ERROR, REMOVE_ERROR } from './mutation-types'

import uniqueId from 'lodash/uniqueId'

const state = {
  container: []
}

const actions = {
  handleError: ({ commit }, message) => {
    commit(HANDLE_ERROR, { id: uniqueId('error_'), message })
  },
  removeError: ({ commit }, id) => {
    commit(REMOVE_ERROR, id)
  }
}

const mutations = {
  [HANDLE_ERROR](state, payload) {
    state.container = [...state.container, payload]
  },
  [REMOVE_ERROR](state, payload) {
    state.container = state.container.filter(erorr => erorr.id !== payload)
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
