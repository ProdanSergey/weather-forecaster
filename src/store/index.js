import Vue from 'vue'
import Vuex from 'vuex'

import errors from '@/store/errors'
import user from '@/store/user'
import forms from '@/store/forms'
import weather from '@/store/weather'
import system from '@/store/system'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    user,
    forms,
    errors,
    weather,
    system
  }
})
