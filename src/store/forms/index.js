import omit from 'lodash/omit'
import get from 'lodash/get'

import {
  MOUNT_FORM,
  UNMOUNT_FORM,
  MOUNT_FORM_FIELD,
  SET_FIELD_VALUE,
  SET_FIELD_CONFIG,
  SET_FIELD_LOADING_STATE,
  SET_FORM_STATE,
  SET_FORM_LOADING_STATE
} from './mutation-types'

import { EMPTY_FORM } from '@/assets/constants'

const state = {
  container: {}
}

const getters = {
  getForm: state => formName => state.container[formName],
  getFormValues: state => (formName, except = []) => {
    const form = get(state, `container.${formName}.fields`, {})
    return Object.entries(form).reduce((map, [fieldName, fieldData]) => {
      if (!except.length || !except.includes(fieldName)) {
        map[fieldName] = fieldData.value
      }
      return map
    }, {})
  },
  getFormState: state => formName =>
    state.container[formName] && state.container[formName].valid,
  getFormLoadingState: state => formName =>
    state.container[formName] && state.container[formName].loading,
  getField: state => (formName, fieldName) =>
    state.container[formName] && state.container[formName].fields[fieldName]
}

const actions = {
  mountForm: ({ commit }, formName) => {
    commit(MOUNT_FORM, formName)
  },
  mountFormField: ({ commit }, payload) => {
    commit(MOUNT_FORM_FIELD, payload)
  },
  unmountForm: ({ commit }, formName) => {
    commit(UNMOUNT_FORM, formName)
  },
  setFormState: ({ commit }, payload) => {
    commit(SET_FORM_STATE, payload)
  },
  setFormLoadingState: ({ commit }, payload) => {
    commit(SET_FORM_LOADING_STATE, payload)
  },
  setFieldValue: ({ commit }, payload) => {
    commit(SET_FIELD_VALUE, payload)
  },
  setFieldConfig: ({ commit }, payload) => {
    commit(SET_FIELD_CONFIG, payload)
  },
  setFieldLoadingState: ({ commit }, payload) => {
    commit(SET_FIELD_LOADING_STATE, payload)
  }
}
const mutations = {
  [MOUNT_FORM](state, formName) {
    state.container = {
      ...state.container,
      [formName]: {
        ...EMPTY_FORM
      }
    }
  },
  [UNMOUNT_FORM](state, formName) {
    state.container = omit(state.container, formName)
  },
  [MOUNT_FORM_FIELD](state, payload) {
    const { formName, fieldName, configuration, initialValue } = payload

    state.container[formName].fields = {
      ...state.container[formName].fields,
      [fieldName]: {
        value: initialValue,
        configuration,
        isLoading: false
      }
    }
  },
  [SET_FIELD_VALUE](state, payload) {
    const { formName, fieldName, value } = payload

    state.container[formName].fields[fieldName] = {
      ...state.container[formName].fields[fieldName],
      value
    }
  },
  [SET_FIELD_CONFIG](state, payload) {
    const { formName, fieldName, configuration } = payload

    state.container[formName].fields[fieldName] = {
      ...state.container[formName].fields[fieldName],
      configuration: { ...configuration }
    }
  },
  [SET_FIELD_LOADING_STATE](state, payload) {
    const { formName, fieldName, isLoading } = payload

    state.container[formName].fields[fieldName] = {
      ...state.container[formName].fields[fieldName],
      isLoading
    }
  },
  [SET_FORM_STATE](state, payload) {
    const { formName, valid } = payload

    state.container[formName].valid = valid
  },
  [SET_FORM_LOADING_STATE](state, payload) {
    const { formName, loading } = payload

    state.container[formName].loading = loading
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
