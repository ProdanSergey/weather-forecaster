export const START_GEOPOSITION_SEARCH = 'startGeopositionSearch'
export const FINISH_GEOPOSITION_SEARCH = 'finishGeopositionSearch'
export const START_ENTITY_FETCH = 'startEntityFetch'
export const FINISH_ENTITY_FETCH = 'finishEntityFetch'

export const SET_ENTITY = 'setEntity'
