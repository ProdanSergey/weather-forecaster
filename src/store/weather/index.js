import {
  START_ENTITY_FETCH,
  FINISH_ENTITY_FETCH,
  SET_ENTITY
} from './mutation-types'

import {
  getLocationByKey,
  getWeatherByGeoCoords,
  getWeatherForecastByLocationId
} from '@/assets/js/apiCalls'

import { ENTITIES } from '@/assets/constants'

const state = {
  loading: {
    location: [],
    weather: [],
    forecast: []
  },
  entities: {
    location: {},
    weather: {},
    forecast: {}
  }
}

const getters = {
  getEntityFetchedState: state => (id, type) => {
    return Object.keys(state.entities[type]).includes(String(id))
  },
  getEntityLoadingState: state => (id, type) => {
    return state.loading[type].includes(id)
  },
  getLocationsPerSubscribtions: (state, getters, rootState) =>
    rootState.user.subscribtions.reduce((locations, id) => {
      const location = state.entities.location[id]
      return location ? [...locations, location] : locations
    }, [])
}

const actions = {
  getLocationsBySubscribtions: ({ dispatch }, ids) => {
    return Promise.all(ids.map(id => dispatch('getLocationById', id)))
  },
  getLocationById: async ({ commit }, id) => {
    const type = ENTITIES.LOCATION

    let location
    try {
      commit(START_ENTITY_FETCH, { id, type })
      location = await getLocationByKey(id)
      commit(SET_ENTITY, { id, type, entity: location })
    } catch (error) {
      throw new Error("Can't fetch location due to API fail")
    } finally {
      commit(FINISH_ENTITY_FETCH, { id, type })
    }

    return location
  },
  getWeatherByLocationCoordinates: async ({ commit }, payload) => {
    const { id, latitude, longitude } = payload
    const type = ENTITIES.WEATHER

    let weather
    try {
      commit(START_ENTITY_FETCH, { id, type })
      weather = await getWeatherByGeoCoords(latitude, longitude)
      commit(SET_ENTITY, { id, type, entity: weather })
    } catch (error) {
      throw new Error("Can't fetch weather due to API fail")
    } finally {
      commit(FINISH_ENTITY_FETCH, { id, type })
    }

    return weather
  },
  getForecastByWeatherId: async ({ commit }, payload) => {
    const { weatherId, locationId: id } = payload
    const type = ENTITIES.FORECAST

    let forecast
    try {
      commit(START_ENTITY_FETCH, { id, type })
      forecast = await getWeatherForecastByLocationId(weatherId)
      commit(SET_ENTITY, { id, type, entity: forecast })
    } catch (error) {
      throw new Error("Can't fetch forecast due to API fail")
    } finally {
      commit(FINISH_ENTITY_FETCH, { id, type })
    }

    return forecast
  }
}

const mutations = {
  [START_ENTITY_FETCH](state, payload) {
    state.loading[payload.type] = [...state.loading[payload.type], payload.id]
  },
  [FINISH_ENTITY_FETCH](state, payload) {
    state.loading[payload.type] = state.loading[payload.type].filter(
      id => id !== payload.id
    )
  },
  [SET_ENTITY](state, payload) {
    const { id, type, entity } = payload

    state.entities[type] = {
      ...state.entities[type],
      [id]: entity
    }
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
