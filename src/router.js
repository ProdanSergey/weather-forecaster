import Vue from 'vue'
import Router from 'vue-router'

import HomePage from '@/views/HomePage'

import UserLogin from '@/components/User/UserLogin'
import UserRegistration from '@/components/User/UserRegistration'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomePage
    },
    {
      path: '/registration',
      name: 'registration',
      component: UserRegistration
    },
    {
      path: '/login',
      name: 'login',
      component: UserLogin
    }
  ]
})
