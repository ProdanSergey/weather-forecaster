import API, { GeoDB } from './api'
import { parseCoordinate } from './parsers'
import { LOCATIONS_TYPES } from '../constants'

export const getLocationsByNamePrefix = namePrefix =>
  GeoDB('geo/cities', { namePrefix })
    .then(response => response.json())
    .then(({ data }) => data)

export const getLocationsByGeo = (
  latitude = 0,
  longitude = 0,
  radius = 10,
  types = LOCATIONS_TYPES.CITY
) =>
  GeoDB(`geo/cities`, {
    location: parseCoordinate(latitude) + parseCoordinate(longitude),
    radius,
    types
  })
    .then(response => response.json())
    .then(({ data }) => data)

export const getLocationByKey = cityId =>
  GeoDB(`geo/cities/${cityId}`)
    .then(response => response.json())
    .then(({ data }) => data)

export const getWeatherByGeoCoords = (lat, lon) =>
  API('weather', { lat, lon }).then(response => response.json())

export const getWeatherForecastByLocationId = locationId =>
  API('forecast', { id: locationId }).then(response => response.json())

export const getWeatherDailyByLocationId = (id, cnt = 5) =>
  API('forecast/daily', { id, cnt }).then(response => response.json())
