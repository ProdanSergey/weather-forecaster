export const getUrlFromBlob = blob =>
  new Promise((resolve, reject) => {
    const reader = new FileReader()

    reader.onload = ({ target }) => resolve(target.result)
    reader.onerror = error => reject(error)

    reader.readAsDataURL(blob)
  })
