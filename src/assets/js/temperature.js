import round from 'lodash/round'
import wrap from 'lodash/fp/wrap'

import { METRICS } from '@/assets/constants'

const getTempInFahrenheit = value => round(((value - 273.15) * 9) / 5 + 32)
const getTempInCelsius = value => round(value - 273.15)
const getTempInKelvin = value => round(value)

const formater = (transform, value, symbol) => `${transform(value)} ${symbol}`

const getTempBasedOnUserMetricConfig = (
  value = 0,
  metric,
  toBeFormated = false
) => {
  let converter
  const symbol = METRICS[metric].SYMBOL

  switch (metric) {
    case METRICS.Celsius.VALUE:
      converter = getTempInCelsius
      break
    case METRICS.Fahrenheit.VALUE:
      converter = getTempInFahrenheit
      break
    default:
      converter = getTempInKelvin
      break
  }

  return toBeFormated
    ? wrap(formater, converter)(value, symbol)
    : converter(value)
}

export default {
  KelvinToFahrenheit: getTempInFahrenheit,
  KelvinToCelsius: getTempInCelsius,
  getTempBasedOnUserMetricConfig
}
