import SCHEMA from '@/assets/constants/schema'
import { DB_NAME, DB_VERSION } from '@/assets/constants'

const openDBConnection = () => {
  return new Promise((resolve, reject) => {
    const db = indexedDB.open(DB_NAME, DB_VERSION)

    db.onsuccess = event => resolve(event.target.result)
    db.onerror = event => reject(event.target.error)
    db.onupgradeneeded = event => {
      const { result: db, transaction } = event.target

      Object.entries(SCHEMA).forEach(
        ([table, { keyPath, autoIncrement, columns }]) => {
          const store = db.createObjectStore(table, { keyPath, autoIncrement })
          columns.forEach(({ indexName, keyPath, options }) =>
            store.createIndex(indexName, keyPath, options)
          )
        }
      )

      transaction.oncomplete = event => resolve(event.target.db)
    }
  })
}

const getStore = (db, tableName, permission) =>
  db.transaction(tableName, permission).objectStore(tableName)

export const getDBEntry = async (tableName, key) => {
  const store = getStore(await openDBConnection(), tableName, 'readonly')

  return new Promise((resolve, reject) => {
    const request = store.get(key)

    request.onsuccess = event => resolve(event.target)
    request.onerror = event => reject(event.target.error)
  })
}

export const getDBEntryByIndex = async (tableName, $index, $value) => {
  const store = getStore(await openDBConnection(), tableName, 'readonly')

  return new Promise((resolve, reject) => {
    const index = store.index($index)
    const request = index.get($value)

    request.onsuccess = event => resolve(event.target)
    request.onerror = event => reject(event.target.error)
  })
}

export const setNewDBEntry = async (tableName, data) => {
  const store = getStore(await openDBConnection(), tableName, 'readwrite')

  return new Promise((resolve, reject) => {
    const request = store.add(data)

    request.onsuccess = event => {
      const { result, source } = event.target
      const value = source.get(result)

      value.onsuccess = event => resolve(event.target.result)
      value.onerror = event => reject(event.target.error)
    }
    request.onerror = event => reject(event.target.error)
  })
}

export const updateDBEntryByIndex = async (tableName, $key, $index, $value) => {
  const store = getStore(await openDBConnection(), tableName, 'readwrite')

  return new Promise((resolve, reject) => {
    const request = store.openCursor()

    request.onsuccess = event => {
      const cursor = event.target.result
      if (cursor && cursor.key === $key) {
        const value = cursor.value
        value[$index] = $value
        const update = cursor.update(value)

        update.onsuccess = event => {
          const result = Object.entries(event.target.source.value)
          resolve(result.find(field => field.includes($index)))
        }
        update.onerror = event => reject(event.target.error)
      } else if (cursor) {
        cursor.continue()
      }
    }
    request.onerror = event => reject(event.target.error)
  })
}
