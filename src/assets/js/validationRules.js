export default {
  required: fieldName => value => !!value || `${fieldName} is required`,
  lessThen: count => value =>
    (value && value.length <= count) || `must be less than ${count} characters`,
  match: pattern => value => pattern.test(value) || 'Value must be valid',
  fieldMatch: (store, formName, path, fieldName) => value =>
    value === store.state.forms.container[formName].fields[path].value ||
    `Value doesn\'t match with ${fieldName} value`
}
