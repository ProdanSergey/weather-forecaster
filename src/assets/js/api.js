export const GeoDB = (path, _params = {}, _options = {}) => {
  const { headers, ...restOptions } = _options

  const url = new URL(`https://wft-geo-db.p.rapidapi.com/v1/${path}`)

  const _headers = new Headers({
    'X-RapidAPI-Host': 'wft-geo-db.p.rapidapi.com',
    'X-RapidAPI-Key': '660c773b7bmsh1a04aec9ac5cc1bp13cadajsnc37a3e9ba8b4',
    ...headers
  })

  const options = {
    method: 'GET',
    headers: _headers,
    ...restOptions
  }

  const params = {
    ..._params
  }

  url.search = new URLSearchParams(params)
  return fetch(url, options)
}

export default (path, _params = {}, _options = {}) => {
  const { headers, ...restOptions } = _options

  const url = new URL(`http://api.openweathermap.org/data/2.5/${path}`)
  const _headers = new Headers({
    ...headers
  })

  const options = {
    method: 'GET',
    headers: _headers,
    ...restOptions
  }

  const params = {
    APPID: '4dddcb3758de27a3676823a4907bf857',
    ..._params
  }

  url.search = new URLSearchParams(params)
  return fetch(url, options)
}
