export const getCityFromResponse = (response = []) => {
  if (response.length) {
    return response.reduce((previousEntity, currentEntity) => {
      if (previousEntity.type === 'CITY') {
        return previousEntity
      }
      return currentEntity.distance < previousEntity.distance
        ? currentEntity
        : previousEntity
    })
  }
}

export const formatIconURL = (icon, large = true, flat = true) => {
  let style = 'w'
  let size = ''

  if (large) size = '@2x'
  if (flat) style = 'wn'

  return `http://openweathermap.org/img/${style}/${icon}${size}.png`
}
