const options = {
  enableHighAccuracy: false,
  timeout: 5000,
  maximumAge: 0
}

export const watchPosition = (
  onStart = () => void 0,
  onFinish = () => void 0
) =>
  new Promise((resolve, reject) => {
    const onSuccess = ({ coords }) => {
      onFinish()
      resolve(coords)
    }

    const onError = error => {
      onFinish()
      reject(error)
    }

    onStart()
    navigator.geolocation.watchPosition(onSuccess, onError, options)
  })

export const getCurrentPosition = (
  onStart = () => void 0,
  onFinish = () => void 0
) =>
  new Promise((resolve, reject) => {
    const onSuccess = ({ coords }) => {
      onFinish()
      resolve(coords)
    }

    const onError = error => {
      onFinish()
      reject(error)
    }

    onStart()
    navigator.geolocation.getCurrentPosition(onSuccess, onError, options)
  })
