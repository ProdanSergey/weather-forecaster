export const SUBSCRIBTION = {
  id: 111289,
  wikiDataId: 'Q1874',
  type: 'CITY',
  city: 'Odessa',
  name: 'Odessa',
  country: 'Ukraine',
  countryCode: 'UA',
  region: 'Odessa Oblast',
  regionCode: '51',
  latitude: 46.47747,
  longitude: 30.73262
}

export const FORECAST = {
  cod: '200',
  message: 0.0086,
  cnt: 40,
  list: [
    {
      dt: 1566745200,
      main: {
        temp: 299.84,
        temp_min: 299.702,
        temp_max: 299.84,
        pressure: 1024.54,
        sea_level: 1024.54,
        grnd_level: 1008.59,
        humidity: 30,
        temp_kf: 0.14
      },
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01d'
        }
      ],
      clouds: {
        all: 0
      },
      wind: {
        speed: 2.98,
        deg: 37.551
      },
      sys: {
        pod: 'd'
      },
      dt_txt: '2019-08-25 15:00:00'
    },
    {
      dt: 1566756000,
      main: {
        temp: 296.3,
        temp_min: 296.2,
        temp_max: 296.3,
        pressure: 1025.96,
        sea_level: 1025.96,
        grnd_level: 1009.84,
        humidity: 48,
        temp_kf: 0.1
      },
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01n'
        }
      ],
      clouds: {
        all: 0
      },
      wind: {
        speed: 3.4,
        deg: 64.567
      },
      sys: {
        pod: 'n'
      },
      dt_txt: '2019-08-25 18:00:00'
    },
    {
      dt: 1566766800,
      main: {
        temp: 293.77,
        temp_min: 293.7,
        temp_max: 293.77,
        pressure: 1026.2,
        sea_level: 1026.2,
        grnd_level: 1010.12,
        humidity: 62,
        temp_kf: 0.07
      },
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01n'
        }
      ],
      clouds: {
        all: 0
      },
      wind: {
        speed: 3.52,
        deg: 74.471
      },
      sys: {
        pod: 'n'
      },
      dt_txt: '2019-08-25 21:00:00'
    },
    {
      dt: 1566777600,
      main: {
        temp: 292.33,
        temp_min: 292.3,
        temp_max: 292.33,
        pressure: 1027.15,
        sea_level: 1027.15,
        grnd_level: 1011.05,
        humidity: 69,
        temp_kf: 0.03
      },
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01n'
        }
      ],
      clouds: {
        all: 0
      },
      wind: {
        speed: 2.5,
        deg: 96.424
      },
      sys: {
        pod: 'n'
      },
      dt_txt: '2019-08-26 00:00:00'
    },
    {
      dt: 1566788400,
      main: {
        temp: 291,
        temp_min: 291,
        temp_max: 291,
        pressure: 1026.53,
        sea_level: 1026.53,
        grnd_level: 1010.43,
        humidity: 72,
        temp_kf: 0
      },
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01n'
        }
      ],
      clouds: {
        all: 0
      },
      wind: {
        speed: 2.09,
        deg: 75.724
      },
      sys: {
        pod: 'n'
      },
      dt_txt: '2019-08-26 03:00:00'
    },
    {
      dt: 1566799200,
      main: {
        temp: 293.7,
        temp_min: 293.7,
        temp_max: 293.7,
        pressure: 1027.01,
        sea_level: 1027.01,
        grnd_level: 1011.06,
        humidity: 60,
        temp_kf: 0
      },
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01d'
        }
      ],
      clouds: {
        all: 0
      },
      wind: {
        speed: 2.41,
        deg: 83.88
      },
      sys: {
        pod: 'd'
      },
      dt_txt: '2019-08-26 06:00:00'
    },
    {
      dt: 1566810000,
      main: {
        temp: 298.246,
        temp_min: 298.246,
        temp_max: 298.246,
        pressure: 1026.7,
        sea_level: 1026.7,
        grnd_level: 1010.79,
        humidity: 43,
        temp_kf: 0
      },
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01d'
        }
      ],
      clouds: {
        all: 0
      },
      wind: {
        speed: 2.86,
        deg: 78.608
      },
      sys: {
        pod: 'd'
      },
      dt_txt: '2019-08-26 09:00:00'
    },
    {
      dt: 1566820800,
      main: {
        temp: 300.612,
        temp_min: 300.612,
        temp_max: 300.612,
        pressure: 1025.77,
        sea_level: 1025.77,
        grnd_level: 1009.76,
        humidity: 33,
        temp_kf: 0
      },
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01d'
        }
      ],
      clouds: {
        all: 5
      },
      wind: {
        speed: 2.68,
        deg: 73.652
      },
      sys: {
        pod: 'd'
      },
      dt_txt: '2019-08-26 12:00:00'
    },
    {
      dt: 1566831600,
      main: {
        temp: 300.112,
        temp_min: 300.112,
        temp_max: 300.112,
        pressure: 1024.28,
        sea_level: 1024.28,
        grnd_level: 1008.11,
        humidity: 34,
        temp_kf: 0
      },
      weather: [
        {
          id: 803,
          main: 'Clouds',
          description: 'broken clouds',
          icon: '04d'
        }
      ],
      clouds: {
        all: 57
      },
      wind: {
        speed: 2.33,
        deg: 68.738
      },
      sys: {
        pod: 'd'
      },
      dt_txt: '2019-08-26 15:00:00'
    },
    {
      dt: 1566842400,
      main: {
        temp: 296.212,
        temp_min: 296.212,
        temp_max: 296.212,
        pressure: 1024.89,
        sea_level: 1024.89,
        grnd_level: 1008.7,
        humidity: 45,
        temp_kf: 0
      },
      weather: [
        {
          id: 802,
          main: 'Clouds',
          description: 'scattered clouds',
          icon: '03n'
        }
      ],
      clouds: {
        all: 28
      },
      wind: {
        speed: 3.76,
        deg: 58.796
      },
      sys: {
        pod: 'n'
      },
      dt_txt: '2019-08-26 18:00:00'
    },
    {
      dt: 1566853200,
      main: {
        temp: 294,
        temp_min: 294,
        temp_max: 294,
        pressure: 1025.05,
        sea_level: 1025.05,
        grnd_level: 1009.03,
        humidity: 50,
        temp_kf: 0
      },
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01n'
        }
      ],
      clouds: {
        all: 0
      },
      wind: {
        speed: 3.24,
        deg: 88.851
      },
      sys: {
        pod: 'n'
      },
      dt_txt: '2019-08-26 21:00:00'
    },
    {
      dt: 1566864000,
      main: {
        temp: 292.5,
        temp_min: 292.5,
        temp_max: 292.5,
        pressure: 1024.84,
        sea_level: 1024.84,
        grnd_level: 1008.8,
        humidity: 56,
        temp_kf: 0
      },
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01n'
        }
      ],
      clouds: {
        all: 0
      },
      wind: {
        speed: 1.87,
        deg: 104.847
      },
      sys: {
        pod: 'n'
      },
      dt_txt: '2019-08-27 00:00:00'
    },
    {
      dt: 1566874800,
      main: {
        temp: 291.4,
        temp_min: 291.4,
        temp_max: 291.4,
        pressure: 1024.32,
        sea_level: 1024.32,
        grnd_level: 1008.3,
        humidity: 58,
        temp_kf: 0
      },
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01n'
        }
      ],
      clouds: {
        all: 0
      },
      wind: {
        speed: 1.08,
        deg: 73.057
      },
      sys: {
        pod: 'n'
      },
      dt_txt: '2019-08-27 03:00:00'
    },
    {
      dt: 1566885600,
      main: {
        temp: 294.596,
        temp_min: 294.596,
        temp_max: 294.596,
        pressure: 1024.64,
        sea_level: 1024.64,
        grnd_level: 1008.75,
        humidity: 48,
        temp_kf: 0
      },
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01d'
        }
      ],
      clouds: {
        all: 0
      },
      wind: {
        speed: 1.53,
        deg: 39.911
      },
      sys: {
        pod: 'd'
      },
      dt_txt: '2019-08-27 06:00:00'
    },
    {
      dt: 1566896400,
      main: {
        temp: 298.412,
        temp_min: 298.412,
        temp_max: 298.412,
        pressure: 1023.98,
        sea_level: 1023.98,
        grnd_level: 1008.32,
        humidity: 41,
        temp_kf: 0
      },
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01d'
        }
      ],
      clouds: {
        all: 0
      },
      wind: {
        speed: 2.72,
        deg: 11.81
      },
      sys: {
        pod: 'd'
      },
      dt_txt: '2019-08-27 09:00:00'
    },
    {
      dt: 1566907200,
      main: {
        temp: 300.188,
        temp_min: 300.188,
        temp_max: 300.188,
        pressure: 1023.16,
        sea_level: 1023.16,
        grnd_level: 1007.5,
        humidity: 36,
        temp_kf: 0
      },
      weather: [
        {
          id: 802,
          main: 'Clouds',
          description: 'scattered clouds',
          icon: '03d'
        }
      ],
      clouds: {
        all: 30
      },
      wind: {
        speed: 3.03,
        deg: 355.833
      },
      sys: {
        pod: 'd'
      },
      dt_txt: '2019-08-27 12:00:00'
    },
    {
      dt: 1566918000,
      main: {
        temp: 299.6,
        temp_min: 299.6,
        temp_max: 299.6,
        pressure: 1022.36,
        sea_level: 1022.36,
        grnd_level: 1006.49,
        humidity: 37,
        temp_kf: 0
      },
      weather: [
        {
          id: 802,
          main: 'Clouds',
          description: 'scattered clouds',
          icon: '03d'
        }
      ],
      clouds: {
        all: 29
      },
      wind: {
        speed: 3.52,
        deg: 1.708
      },
      sys: {
        pod: 'd'
      },
      dt_txt: '2019-08-27 15:00:00'
    },
    {
      dt: 1566928800,
      main: {
        temp: 296.561,
        temp_min: 296.561,
        temp_max: 296.561,
        pressure: 1022.85,
        sea_level: 1022.85,
        grnd_level: 1006.76,
        humidity: 50,
        temp_kf: 0
      },
      weather: [
        {
          id: 801,
          main: 'Clouds',
          description: 'few clouds',
          icon: '02n'
        }
      ],
      clouds: {
        all: 22
      },
      wind: {
        speed: 2.85,
        deg: 21.854
      },
      sys: {
        pod: 'n'
      },
      dt_txt: '2019-08-27 18:00:00'
    },
    {
      dt: 1566939600,
      main: {
        temp: 294.5,
        temp_min: 294.5,
        temp_max: 294.5,
        pressure: 1023.46,
        sea_level: 1023.46,
        grnd_level: 1007.45,
        humidity: 63,
        temp_kf: 0
      },
      weather: [
        {
          id: 801,
          main: 'Clouds',
          description: 'few clouds',
          icon: '02n'
        }
      ],
      clouds: {
        all: 16
      },
      wind: {
        speed: 2.62,
        deg: 15.094
      },
      sys: {
        pod: 'n'
      },
      dt_txt: '2019-08-27 21:00:00'
    },
    {
      dt: 1566950400,
      main: {
        temp: 292.693,
        temp_min: 292.693,
        temp_max: 292.693,
        pressure: 1023.28,
        sea_level: 1023.28,
        grnd_level: 1007.25,
        humidity: 72,
        temp_kf: 0
      },
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01n'
        }
      ],
      clouds: {
        all: 8
      },
      wind: {
        speed: 2.51,
        deg: 31.085
      },
      sys: {
        pod: 'n'
      },
      dt_txt: '2019-08-28 00:00:00'
    },
    {
      dt: 1566961200,
      main: {
        temp: 291.3,
        temp_min: 291.3,
        temp_max: 291.3,
        pressure: 1023.42,
        sea_level: 1023.42,
        grnd_level: 1007.36,
        humidity: 76,
        temp_kf: 0
      },
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01n'
        }
      ],
      clouds: {
        all: 0
      },
      wind: {
        speed: 2.03,
        deg: 23.046
      },
      sys: {
        pod: 'n'
      },
      dt_txt: '2019-08-28 03:00:00'
    },
    {
      dt: 1566972000,
      main: {
        temp: 293.104,
        temp_min: 293.104,
        temp_max: 293.104,
        pressure: 1023.72,
        sea_level: 1023.72,
        grnd_level: 1007.82,
        humidity: 67,
        temp_kf: 0
      },
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01d'
        }
      ],
      clouds: {
        all: 0
      },
      wind: {
        speed: 2.76,
        deg: 24.705
      },
      sys: {
        pod: 'd'
      },
      dt_txt: '2019-08-28 06:00:00'
    },
    {
      dt: 1566982800,
      main: {
        temp: 296.816,
        temp_min: 296.816,
        temp_max: 296.816,
        pressure: 1023.34,
        sea_level: 1023.34,
        grnd_level: 1007.51,
        humidity: 46,
        temp_kf: 0
      },
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01d'
        }
      ],
      clouds: {
        all: 0
      },
      wind: {
        speed: 3.74,
        deg: 30.421
      },
      sys: {
        pod: 'd'
      },
      dt_txt: '2019-08-28 09:00:00'
    },
    {
      dt: 1566993600,
      main: {
        temp: 299.015,
        temp_min: 299.015,
        temp_max: 299.015,
        pressure: 1022.14,
        sea_level: 1022.14,
        grnd_level: 1006.35,
        humidity: 34,
        temp_kf: 0
      },
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01d'
        }
      ],
      clouds: {
        all: 0
      },
      wind: {
        speed: 3.48,
        deg: 22.872
      },
      sys: {
        pod: 'd'
      },
      dt_txt: '2019-08-28 12:00:00'
    },
    {
      dt: 1567004400,
      main: {
        temp: 298.206,
        temp_min: 298.206,
        temp_max: 298.206,
        pressure: 1021.87,
        sea_level: 1021.87,
        grnd_level: 1005.84,
        humidity: 34,
        temp_kf: 0
      },
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01d'
        }
      ],
      clouds: {
        all: 0
      },
      wind: {
        speed: 3.72,
        deg: 40.275
      },
      sys: {
        pod: 'd'
      },
      dt_txt: '2019-08-28 15:00:00'
    },
    {
      dt: 1567015200,
      main: {
        temp: 294.775,
        temp_min: 294.775,
        temp_max: 294.775,
        pressure: 1022.81,
        sea_level: 1022.81,
        grnd_level: 1006.41,
        humidity: 45,
        temp_kf: 0
      },
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01n'
        }
      ],
      clouds: {
        all: 0
      },
      wind: {
        speed: 3.77,
        deg: 65.08
      },
      sys: {
        pod: 'n'
      },
      dt_txt: '2019-08-28 18:00:00'
    },
    {
      dt: 1567026000,
      main: {
        temp: 291.465,
        temp_min: 291.465,
        temp_max: 291.465,
        pressure: 1023.74,
        sea_level: 1023.74,
        grnd_level: 1007.58,
        humidity: 58,
        temp_kf: 0
      },
      weather: [
        {
          id: 802,
          main: 'Clouds',
          description: 'scattered clouds',
          icon: '03n'
        }
      ],
      clouds: {
        all: 27
      },
      wind: {
        speed: 4.33,
        deg: 81.503
      },
      sys: {
        pod: 'n'
      },
      dt_txt: '2019-08-28 21:00:00'
    },
    {
      dt: 1567036800,
      main: {
        temp: 289.312,
        temp_min: 289.312,
        temp_max: 289.312,
        pressure: 1024.01,
        sea_level: 1024.01,
        grnd_level: 1007.99,
        humidity: 55,
        temp_kf: 0
      },
      weather: [
        {
          id: 802,
          main: 'Clouds',
          description: 'scattered clouds',
          icon: '03n'
        }
      ],
      clouds: {
        all: 43
      },
      wind: {
        speed: 3.75,
        deg: 101.49
      },
      sys: {
        pod: 'n'
      },
      dt_txt: '2019-08-29 00:00:00'
    },
    {
      dt: 1567047600,
      main: {
        temp: 287.038,
        temp_min: 287.038,
        temp_max: 287.038,
        pressure: 1024.98,
        sea_level: 1024.98,
        grnd_level: 1008.81,
        humidity: 56,
        temp_kf: 0
      },
      weather: [
        {
          id: 801,
          main: 'Clouds',
          description: 'few clouds',
          icon: '02n'
        }
      ],
      clouds: {
        all: 13
      },
      wind: {
        speed: 3.45,
        deg: 91.327
      },
      sys: {
        pod: 'n'
      },
      dt_txt: '2019-08-29 03:00:00'
    },
    {
      dt: 1567058400,
      main: {
        temp: 288.4,
        temp_min: 288.4,
        temp_max: 288.4,
        pressure: 1025.04,
        sea_level: 1025.04,
        grnd_level: 1008.98,
        humidity: 50,
        temp_kf: 0
      },
      weather: [
        {
          id: 801,
          main: 'Clouds',
          description: 'few clouds',
          icon: '02d'
        }
      ],
      clouds: {
        all: 20
      },
      wind: {
        speed: 3.51,
        deg: 91.046
      },
      sys: {
        pod: 'd'
      },
      dt_txt: '2019-08-29 06:00:00'
    },
    {
      dt: 1567069200,
      main: {
        temp: 293.225,
        temp_min: 293.225,
        temp_max: 293.225,
        pressure: 1025.23,
        sea_level: 1025.23,
        grnd_level: 1009.26,
        humidity: 32,
        temp_kf: 0
      },
      weather: [
        {
          id: 803,
          main: 'Clouds',
          description: 'broken clouds',
          icon: '04d'
        }
      ],
      clouds: {
        all: 55
      },
      wind: {
        speed: 3.57,
        deg: 92.7
      },
      sys: {
        pod: 'd'
      },
      dt_txt: '2019-08-29 09:00:00'
    },
    {
      dt: 1567080000,
      main: {
        temp: 295.958,
        temp_min: 295.958,
        temp_max: 295.958,
        pressure: 1023.95,
        sea_level: 1023.95,
        grnd_level: 1007.84,
        humidity: 24,
        temp_kf: 0
      },
      weather: [
        {
          id: 802,
          main: 'Clouds',
          description: 'scattered clouds',
          icon: '03d'
        }
      ],
      clouds: {
        all: 39
      },
      wind: {
        speed: 3.4,
        deg: 90.977
      },
      sys: {
        pod: 'd'
      },
      dt_txt: '2019-08-29 12:00:00'
    },
    {
      dt: 1567090800,
      main: {
        temp: 295.458,
        temp_min: 295.458,
        temp_max: 295.458,
        pressure: 1023.37,
        sea_level: 1023.37,
        grnd_level: 1007.07,
        humidity: 25,
        temp_kf: 0
      },
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01d'
        }
      ],
      clouds: {
        all: 0
      },
      wind: {
        speed: 3.39,
        deg: 90.727
      },
      sys: {
        pod: 'd'
      },
      dt_txt: '2019-08-29 15:00:00'
    },
    {
      dt: 1567101600,
      main: {
        temp: 292.017,
        temp_min: 292.017,
        temp_max: 292.017,
        pressure: 1024.01,
        sea_level: 1024.01,
        grnd_level: 1007.54,
        humidity: 34,
        temp_kf: 0
      },
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01n'
        }
      ],
      clouds: {
        all: 1
      },
      wind: {
        speed: 3.73,
        deg: 81.381
      },
      sys: {
        pod: 'n'
      },
      dt_txt: '2019-08-29 18:00:00'
    },
    {
      dt: 1567112400,
      main: {
        temp: 288.934,
        temp_min: 288.934,
        temp_max: 288.934,
        pressure: 1024.7,
        sea_level: 1024.7,
        grnd_level: 1008.52,
        humidity: 47,
        temp_kf: 0
      },
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01n'
        }
      ],
      clouds: {
        all: 0
      },
      wind: {
        speed: 4.23,
        deg: 104.375
      },
      sys: {
        pod: 'n'
      },
      dt_txt: '2019-08-29 21:00:00'
    },
    {
      dt: 1567123200,
      main: {
        temp: 287.2,
        temp_min: 287.2,
        temp_max: 287.2,
        pressure: 1024.73,
        sea_level: 1024.73,
        grnd_level: 1008.49,
        humidity: 53,
        temp_kf: 0
      },
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01n'
        }
      ],
      clouds: {
        all: 0
      },
      wind: {
        speed: 3.27,
        deg: 111.17
      },
      sys: {
        pod: 'n'
      },
      dt_txt: '2019-08-30 00:00:00'
    },
    {
      dt: 1567134000,
      main: {
        temp: 286,
        temp_min: 286,
        temp_max: 286,
        pressure: 1024.88,
        sea_level: 1024.88,
        grnd_level: 1008.44,
        humidity: 56,
        temp_kf: 0
      },
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01n'
        }
      ],
      clouds: {
        all: 0
      },
      wind: {
        speed: 2.71,
        deg: 113.969
      },
      sys: {
        pod: 'n'
      },
      dt_txt: '2019-08-30 03:00:00'
    },
    {
      dt: 1567144800,
      main: {
        temp: 288.416,
        temp_min: 288.416,
        temp_max: 288.416,
        pressure: 1025.03,
        sea_level: 1025.03,
        grnd_level: 1009.03,
        humidity: 48,
        temp_kf: 0
      },
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01d'
        }
      ],
      clouds: {
        all: 6
      },
      wind: {
        speed: 2.74,
        deg: 112.388
      },
      sys: {
        pod: 'd'
      },
      dt_txt: '2019-08-30 06:00:00'
    },
    {
      dt: 1567155600,
      main: {
        temp: 293.5,
        temp_min: 293.5,
        temp_max: 293.5,
        pressure: 1025.26,
        sea_level: 1025.26,
        grnd_level: 1009.47,
        humidity: 33,
        temp_kf: 0
      },
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01d'
        }
      ],
      clouds: {
        all: 4
      },
      wind: {
        speed: 2.57,
        deg: 110.783
      },
      sys: {
        pod: 'd'
      },
      dt_txt: '2019-08-30 09:00:00'
    },
    {
      dt: 1567166400,
      main: {
        temp: 296.2,
        temp_min: 296.2,
        temp_max: 296.2,
        pressure: 1024.44,
        sea_level: 1024.44,
        grnd_level: 1008.55,
        humidity: 26,
        temp_kf: 0
      },
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01d'
        }
      ],
      clouds: {
        all: 2
      },
      wind: {
        speed: 2.52,
        deg: 103.727
      },
      sys: {
        pod: 'd'
      },
      dt_txt: '2019-08-30 12:00:00'
    }
  ],
  city: {
    id: 708695,
    name: 'Gnedin',
    coord: {
      lat: 50.329,
      lon: 30.7106
    },
    country: 'UA',
    timezone: 10800,
    sunrise: 1566702030,
    sunset: 1566752322
  }
}

export const DAILY_FORECAST = {
  cod: '200',
  message: 0.0032,
  city: {
    id: 1851632,
    name: 'Shuzenji',
    coord: { lon: 138.933334, lat: 34.966671 },
    country: 'JP',
    timezone: 32400
  },
  cnt: 10,
  list: [
    {
      dt: 1406080800,
      temp: {
        day: 297.77,
        min: 293.52,
        max: 297.77,
        night: 293.52,
        eve: 297.77,
        morn: 297.77
      },
      pressure: 925.04,
      humidity: 76,
      weather: [
        { id: 803, main: 'Clouds', description: 'broken clouds', icon: '04d' }
      ]
    },
    {
      dt: 1406080801,
      temp: {
        day: 297.77,
        min: 293.52,
        max: 297.77,
        night: 293.52,
        eve: 297.77,
        morn: 297.77
      },
      pressure: 925.04,
      humidity: 76,
      weather: [
        { id: 803, main: 'Clouds', description: 'broken clouds', icon: '04d' }
      ]
    },
    {
      dt: 1406080802,
      temp: {
        day: 297.77,
        min: 293.52,
        max: 297.77,
        night: 293.52,
        eve: 297.77,
        morn: 297.77
      },
      pressure: 925.04,
      humidity: 76,
      weather: [
        { id: 803, main: 'Clouds', description: 'broken clouds', icon: '04d' }
      ]
    },
    {
      dt: 1406080803,
      temp: {
        day: 297.77,
        min: 293.52,
        max: 297.77,
        night: 293.52,
        eve: 297.77,
        morn: 297.77
      },
      pressure: 925.04,
      humidity: 76,
      weather: [
        { id: 803, main: 'Clouds', description: 'broken clouds', icon: '04d' }
      ]
    },
    {
      dt: 1406080804,
      temp: {
        day: 297.77,
        min: 293.52,
        max: 297.77,
        night: 293.52,
        eve: 297.77,
        morn: 297.77
      },
      pressure: 925.04,
      humidity: 76,
      weather: [
        { id: 803, main: 'Clouds', description: 'broken clouds', icon: '04d' }
      ]
    }
  ]
}

export const WEATHER_DATA = {
  coord: {
    lon: 30.71,
    lat: 50.33
  },
  weather: [
    {
      id: 501,
      main: 'Rain',
      description: 'moderate rain',
      icon: '10n'
    },
    {
      id: 201,
      main: 'Thunderstorm',
      description: 'thunderstorm with rain',
      icon: '11n'
    }
  ],
  base: 'stations',
  main: {
    temp: 300.47,
    pressure: 1025,
    humidity: 30,
    temp_min: 300.15,
    temp_max: 301.15
  },
  visibility: 10000,
  wind: {
    speed: 2
  },
  clouds: {
    all: 0
  },
  dt: 1566740624,
  sys: {
    type: 1,
    id: 8898,
    message: 0.0079,
    country: 'UA',
    sunrise: 1566702030,
    sunset: 1566752322
  },
  timezone: 10800,
  id: 708695,
  name: 'Gnedin',
  cod: 200
}

export const SEARCH = [
  {
    city: 'Odesskoye',
    country: 'Russia',
    countryCode: 'RU',
    id: 104248,
    latitude: 54.215,
    longitude: 72.9651,
    name: 'Odesskoye',
    region: 'Omsk Oblast',
    regionCode: 'OMS',
    type: 'CITY'
  },
  {
    city: 'Odessa',
    country: 'Ukraine',
    countryCode: 'UA',
    id: 111289,
    latitude: 46.47747,
    longitude: 30.73262,
    name: 'Odessa',
    region: 'Odessa Oblast',
    regionCode: '51',
    type: 'CITY',
    wikiDataId: 'Q1874'
  }
]

export const GPS_POINTS = [
  {
    id: 112482,
    wikiDataId: 'Q4140630',
    type: 'CITY',
    city: 'Hnidyn',
    name: 'Hnidyn',
    country: 'Ukraine',
    countryCode: 'UA',
    region: 'Kyiv Oblast',
    regionCode: '32',
    latitude: 50.32899,
    longitude: 30.71059,
    distance: 5.93
  },
  {
    id: 111322,
    type: 'CITY',
    city: 'Kyiv',
    name: 'Kyiv',
    country: 'Ukraine',
    countryCode: 'UA',
    region: 'Kiev',
    regionCode: '30',
    latitude: 50.45466,
    longitude: 30.5238,
    distance: 6.04
  },
  {
    id: 112486,
    wikiDataId: 'Q4380462',
    type: 'CITY',
    city: 'Prolisky',
    name: 'Prolisky',
    country: 'Ukraine',
    countryCode: 'UA',
    region: 'Kiev',
    regionCode: '30',
    latitude: 50.39118,
    longitude: 30.78,
    distance: 6.99
  },
  {
    id: 111138,
    wikiDataId: 'Q4500392',
    type: 'CITY',
    city: 'Khotiv',
    name: 'Khotiv',
    country: 'Ukraine',
    countryCode: 'UA',
    region: 'Kyiv Oblast',
    regionCode: '32',
    latitude: 50.33069,
    longitude: 30.46836,
    distance: 8.01
  },
  {
    id: 111487,
    wikiDataId: 'Q4224885',
    type: 'CITY',
    city: 'Knyazhichi',
    name: 'Knyazhichi',
    country: 'Ukraine',
    countryCode: 'UA',
    region: 'Kyiv Oblast',
    regionCode: '32',
    latitude: 50.46275,
    longitude: 30.78369,
    distance: 8.61
  }
]

export const WEATHER_DATA_MOCK = {
  entities: {
    locations: {
      112482: GPS_POINTS[0]
    },
    weather: {
      112482: WEATHER_DATA
    },
    forecast: {
      112482: FORECAST
    },
    daily: {
      112482: DAILY_FORECAST
    }
  }
}
