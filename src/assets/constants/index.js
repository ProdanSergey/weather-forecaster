export const DB_NAME = 'vueApp'
export const DB_VERSION = 1

export const EMPTY_FORM = {
  fields: {},
  loading: false,
  valid: false
}

export const METRICS = {
  Celsius: {
    VALUE: 'Celsius',
    SYMBOL: '°C'
  },
  Fahrenheit: {
    VALUE: 'Fahrenheit',
    SYMBOL: '°F'
  },
  Kelvin: {
    VALUE: 'Kelvin',
    SYMBOL: 'K'
  }
}

export const DIALOG_BOX_SIZES = {
  medium: 450,
  large: 600,
  xLarge: '50rem'
}

export const LOCATIONS_TYPES = {
  CITY: 'CITY'
}

export const ENTITIES = {
  LOCATION: 'location',
  WEATHER: 'weather',
  FORECAST: 'forecast'
}

export const WEATHER_ATTRIBUTES_MAP = {
  grnd_level: {
    title: 'Ground Level',
    icon: 'fa-buffer',
    metric: 'hPa'
  },
  humidity: {
    title: 'Humidity',
    icon: 'fa-tint',
    metric: '%'
  },
  pressure: {
    title: 'Pressure',
    icon: 'fa-tachometer-alt',
    metric: 'hPa',
    description: 'Atmospheric pressure in general.'
  },
  sea_level: {
    title: 'Sea Level',
    icon: 'fa-water',
    metric: 'hPa',
    description: 'Atmospheric pressure on the sea level.'
  },
  temp: {
    title: 'Temperature',
    icon: 'fa-temperature-half',
    description: 'Temperature at the moment.'
  },
  temp_max: {
    title: 'Max. Temperature',
    icon: 'fa-temperature-high',
    description: 'Maximum temperature at the moment.'
  },
  temp_min: {
    title: 'Sea Level',
    icon: 'fa-temperature-low',
    description: 'Minimum temperature at the moment.'
  }
}

export const GET_GEOPOSITION_TIME_OUT_CODE = 3
export const DEFAULT_CITY = 698740

export const FORMS = {
  REG_FORM_NAME: 'RegForm',
  LOGIN_FORM: 'LoginForm',
  LOCATION_SEARCH_FORM: 'LocationSearchForm',
  HEADER_MENU_FORM: 'HeaderMenuForm',
  ADD_NEW_LOCATION_FORM: 'AddNewLocationForm'
}
