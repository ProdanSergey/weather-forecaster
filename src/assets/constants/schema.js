export default {
  user: {
    keyPath: 'id',
    autoIncrement: true,
    columns: [
      {
        indexName: 'firstName',
        keyPath: 'firstName',
        options: { unique: false }
      },
      {
        indexName: 'lastName',
        keyPath: 'lastName',
        options: { unique: false }
      },
      {
        indexName: 'email',
        keyPath: 'email',
        options: { unique: true }
      },
      {
        indexName: 'password',
        keyPath: 'password',
        options: { unique: false }
      },
      {
        indexName: 'metric',
        keyPath: 'metric',
        options: { unique: false }
      },
      {
        indexName: 'avatar',
        keyPath: 'avatar',
        options: { unique: false }
      },
      {
        indexName: 'subscribtions',
        keyPath: 'subscribtions',
        options: { unique: false }
      }
    ]
  }
}
