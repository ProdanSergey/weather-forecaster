import Vue from 'vue'
import './plugins/vuetify'

import App from './App.vue'
import router from './router'
import store from '@/store'

import { METRICS } from '@/assets/constants'

Vue.config.productionTip = false

Vue.prototype.$metrics = METRICS

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
