import { mapActions, mapGetters } from 'vuex'

export const BaseFormFieldMixin = {
  inheritAttrs: false,
  props: {
    ownProps: {
      type: Object,
      default: () => ({})
    },
    configuration: {
      type: Object,
      default: () => ({})
    },
    initialValue: {
      type: undefined
    }
  },
  watch: {
    initialValue(value, previous) {
      if (value !== previous) this.value = value
    }
  },
  computed: {
    ...mapGetters({
      getField: 'forms/getField'
    }),
    value: {
      set: function(value) {
        this.setFieldValue({
          formName: this.$parent.$attrs.name,
          fieldName: this.$attrs.name,
          value
        })

        this.$emit('change', value)
      },
      get: function() {
        const field = this.getField(this.$parent.$attrs.name, this.$attrs.name)

        return field && field.value
      }
    },
    loading() {
      const field = this.getField(this.$parent.$attrs.name, this.$attrs.name)

      return field && field.isLoading
    }
  },
  methods: {
    ...mapActions({
      mountFormField: 'forms/mountFormField',
      setFieldValue: 'forms/setFieldValue',
      setFieldConfig: 'forms/setFieldConfig',
      setFieldLoadingState: 'forms/setFieldLoadingState'
    })
  },
  created() {
    this.mountFormField({
      formName: this.$parent.$attrs.name,
      fieldName: this.$attrs.name,
      configuration: this.configuration,
      initialValue: this.initialValue
    })
  }
}
