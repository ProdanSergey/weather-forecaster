# weather-forecaster

Basic weather forecast app built with Vue.js powered up with Vuetify Material library. It gives the ability to track locations weather by using OpenWeathefAPI. Provide personal account feature, GPS location detection, and store data using IndexedDB. 

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your unit tests
```
npm run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
