module.exports = {
  css: {
    loaderOptions: {
      sass: {
        data: `
          @import "@/assets/scss/_variables.scss";
          @import "include-media/dist/_include-media.scss";
        `
      }
    }
  },
  lintOnSave: false
}
